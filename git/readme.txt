
# git subtree
https://segmentfault.com/a/1190000003969060
http://cssor.com/git-subtree-usage.html
git remote add -f planetchain.github.io https://github.com/planetchain/planetchain.github.io.git
git subtree add --prefix=public planetchain.github.io master
git subtree pull --prefix=public planetchain.github.io master
git subtree push --prefix=public planetchain.github.io master

# see remote
> git remote -v
bn	ssh://dhabspgit02.barnesandnoble.com:29418/kernel/omap.git (fetch)
bn	ssh://dhabspgit02.barnesandnoble.com:29418/kernel/omap.git (push) 

# push to jerrit 
> git push bn bngb03:refs/for/bngb03
# push to specific jerrit id
git push bn recovery:refs/changes/4306
# push to jerrit and add topic
> git push bn bngb03:refs/for/bngb03/evt0b1


# push to git storage directly
> git push bn bngb03:refs/bn/bngb03

# amend the latest commit
> git commit --amend 

# see the specific file of specifi commit id
> git show  476dd8f7f0342868d49791aa0b4c073d8dfc451  host_platform.h

# see the modified files of specifi commit id
> git show  476dd8f7f0342868d49791aa0b4c073d8dfc451 --stat

# patches form specific commit to HEAD 
> git format-patch 0ef939d88258eec69c63f4281783feccd52836ac

# see modifies
> git diff

# see latest 3 commints
> git log -n3

# see changed files
> git log --stat

# see the differences between two commit
> git log --pretty=oneline
c2fc8223c78ad13f5731139a14cf1ebfe7efa44c Configuration file for Ovation (no cleanup on defconfig)
63c0226c43ad0e577fd4dfd1f75e50ae370cd4ce Ignore the intermediate files in arch/arm/boot/compressed
0ef939d88258eec69c63f4281783feccd52836ac SGX: UDD: Restore sched_update for manual update panels
0089d9c24f37e574ad0786f61db58a55e499bb98 android_4430_defconfig: Enable FUSE filesystem
772f184ae3e5c208da4def24e3bef478f134eb47 misc: twl6040-vib: Fix haptic feedback
> git diff 0089d9c24f37e574ad0786f61db58a55e499bb98 772f184ae3e5c208da4def24e3bef478f134eb47 
> git diff HEAD^..HEAD

# abort all the modifies
> git reset HEAD

# abort specific file's modifies
> git checkout file.txt

# foget to add some files in last commit
> git reset --soft HEAD^
> git add foget.txt
> git commit -a

# download source code
> repo init -u ssh://dhabspgit02.barnesandnoble.com:29418/platform/omapmanifest.git -b bngb02

# resovle conflicts while push to storage from jerrit 
# create a new branch
> git checkout -b local bn/bngb04
# sync to get newest from storage
> repo sync .
# fetch the last change in jerrit
> git fetch ssh://fjin@pa-git02.barnesandnoble.com:29418/platform/vendor/bn/build/scripts refs/changes/42/442/1 && git cherry-pick FETCH_HEAD
# upload your change
> repo upload

# restore deleted dir in external
# get the repo site(wpa_supplicant_6) from defualt.xml
jfeng@ubuntu:~/Project/ovation/src/pa-git02_bngb04/external$ vi ../.repo/manifests/default.xml 
# repo sync
jfeng@ubuntu:~/Project/ovation/src/pa-git02_bngb04/external$ repo sync platform/external/wpa_supplicant_6

# see graph of branch
> git log --graph --oneline --all
> gitk --all
> gitk bngb04 bn/bngb04


# merge pa-git02 to dhabsggit02
 2044  git remote -v
 2045  git remote add -f in ssh://dhabspgit02.barnesandnoble:29418/platform/hardware/ti/wlan.git
 2046  git remote add -f in ssh://dhabspgit02.barnesandnoble.com:29418/platform/hardware/ti/wlan.git
 2047  git remote rm in
 2048  git remote add -f in ssh://dhabspgit02.barnesandnoble.com:29418/platform/hardware/ti/wlan.git
 2049  ls
 2050  git remote -v
 2051  git branch -a
 2052  git checkout -b bngb04.in in/bngb04
 2053  git branch -a

# traverse all repositories
> repo forall -c "git checkout bn/master"

# If you want to commit on top of the current HEAD with the exact state at a different commit, 
# undoing all the intermediate commits, then you can use reset to create the correct state of 
# 0the index to make the commit.reset the index to the desired tree
> git reset 56e05fced
# move the branch pointer back to the previous HEAD
> git reset --soft HEAD@{1}
> git commit -m "Revert to 56e05fced"
# Update working copy to reflect the new commit
> git reset --hard

# get patchs "--start-number"
> git fetch ssh://fjin@pa-git02.barnesandnoble.com:29418/ics/kernel/omap refs/changes/97/897/1 && git format-patch -1 --start-number 1     FETCH_HEAD
> git fetch ssh://fjin@pa-git02.barnesandnoble.com:29418/ics/kernel/omap refs/changes/00/900/1 && git format-patch -1 --start-number 2     FETCH_HEAD
0001-OVATION-ICS-Enable-TWL6040-6041-audio.patch
0002-Add-ATMEL-MXT-touchpanel-support-for-new-touchpanel-.patch

# grep
> git grep 2[.]3
> git grep -e SYSCALL_DEFINE --and -e reboot

# 
repo forall -c  git checkout -b ${BRANCH_NAME} bn/${BRANCH_NAME}
repo forall -c  git checkout -b bnics05_hum_evt1a bn/bnics05_hum_evt1a
repo forall -c  git checkout bnics05_hum_evt1a
repo forall -c  git merge bnics05_ova_evt1b

# show deleted files
git log --diff-filter=D --summary

# show deleted lien
http://stackoverflow.com/questions/4404444/how-do-i-blame-a-deleted-line





repo forall -c  "pwd; git reset --hard HEAD"
repo forall -c  "pwd; git checkout tmp"
repo forall -c  "pwd; git branch -D bnics05_hum_evt1a"
repo forall -c  "pwd; git checkout -b bnics05_hum_evt1a bn/bnics05_hum_evt1a"
repo forall -c  "pwd; git merge bn/bnics05_ova_evt1b"




# remove staged files
git rm -r --cached .
	
# see what will be added
git add -n .

# repo
repo init -u ssh://dhabspgit02.barnesandnoble.com:29418/platform/omapmanifest.git -b bngb02


# erro
gpg: Signature made Tue 11 Jan 2011 05:27:40 AM CST using DSA key ID 920F5C65
gpg: Can't check signature: public key not found

cd /home/jfeng/Project/tool
rm repo
ln -s repo_google repo
rm -rf /home/jfeng/.repoconfig/

repo init -u git://git.omapzoom.org/repo/android/platform/omapmanifest.git -b 27.x -m RLS27.IS.1_IcecreamSandwich.xml --repo-branch=master --repo-url=git://git.omapzoom.org/tools/repo


o# setup remote repositories
:git branch --set-upstream master origin/master


# How to use git send-email
1. git send-email is included in an individual package, named "git-email":
$ sudo apt-get install git-email

2. Configure the SMTP server info after the installation:
$ git config --global sendemail.smtpserver smtp.gmail.com
$ git config --global sendemail.smtpserverport 587
$ git config --global sendemail.smtpencryption tls
$ git config --global sendemail.smtpuser jin88.feng@gmail.com

3. Format a patch and send it out to the receiver:
$ git send-email --compose --no-chain-reply-to --suppress-from --to kernel@kernel.org 0001-*.patch

# rmove all the local branch
git branch --merged | grep -v "\*" | xargs -n 1 git branch -d
repo forall -c 'pwd; git branch | grep -v "\*" | xargs -n 1 git branch -D'



> github  sync the forked the repo
https://help.github.com/articles/fork-a-repo





http://d.hatena.ne.jp/kinneko/20120730/p18
git daemon --reuseaddr --base-path=/home/jfeng/tmp/ --export-all


git tag -a v0.0.1
git push ext v0.0.1 HEAD:refs/heads/ip31



# publish-local-repository
## remote
fengjin@ubuntu:~$ mkdir adas.git
fengjin@ubuntu:~$ cd adas.git/
fengjin@ubuntu:~/adas.git$ git --bare init
## local
jfeng@jfeng-HP-ProBook-640-G1:adas$ git remote add origin ssh://fengjin@10.25.20.14/home/fengjin/adas.git
jfeng@jfeng-HP-ProBook-640-G1:adas$ git push -u origin master


# add only modfied and deleted files, ignore untracked files
git commit -a
git add -u

