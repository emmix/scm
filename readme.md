

# Bob
https://github.com/BobBuildTool/bob
https://media.readthedocs.org/pdf/bob-build-tool/latest/bob-build-tool.pdf


git
=======================
<Version Control with Git>

repo
=======================


Branching strategy and release management
=======================
http://nvie.com/posts/a-successful-git-branching-model/

github
=======================
http://www.cnblogs.com/haore147/p/4219673.html


Requirement for building facility:
=================================

1. can choose x86(pc) or arm(board) in each package
2. build outputs are in out/x86(arm)/package1 and out/x86(arm)/packge2 respectively
3. A project is a group of packages, which is physically managed by repo and logical dependencies and buildings are maintained by coppe packages
4. A package called projects is used to maintain which project is consisted of which packages. Actually this package is a set of repo's manifest files, each represents a project


Make
=================================
https://www.gnu.org/software/make/manual/make.html
<GNU Make Manual>
<Mange project with GNU Make>

CMake
=================================
https://cmake.org/documentation/
<cmake-practice.pdf>
<cmake-rules.pdf>
<mastering cmake>

Ref
=======================

http://wenku.baidu.com/view/2b736a05b84ae45c3b358cca.html
http://blog.csdn.net/felix_f/article/details/8902626
