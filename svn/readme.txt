

http://odyniec.net/articles/ubuntu-subversion-server/

# discard local changes
http://svnbook.red-bean.com/en/1.7/svn.ref.svn.c.revert.html
$ svn revert foo.c
$ svn revert --depth=infinity .

svn diff -x -uw -r r226:r227 | colordiff
svn diff -x -uw -r r226:r227 main.cpp | colordiff
