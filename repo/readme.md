

# The format of manifest xml
$ repo help manifest


Paste the commands to sync nvidia source code and to build:

For the internal nookjb02 branch, the repo init command is the following:

   repo init -u ssh://dhabspgit02.barnesandnoble.com:29418/nvidia/manifest  -b nookjb02
   repo sync -j4
   repo forall -c git checkout -b nookjb02 bn/nookjb02

The scripts to build are:

   vendor/bn/build/scripts/setup_env.sh
   vendor/bn/build/scripts/build_script.sh


For the external nookjb02 branch,  please upload your ssh key first and repo init command is the following:

   repo init -u ssh://pa-git03.barnesandnoble.com:29418/nvidia/manifest  -b nookjb02
   repo sync -j4
   repo forall -c git checkout -b nookjb02 bn/nookjb02

For building the external branch, we did not expose our scripts.  They are just the manual commands:

    vendor/bn/build/scripts/Build.sh 



# glue framework and bsp  to generate erdf
vendor/bn/build/scripts/glue.sh -t penguin -M -E -V -b bnics05_pen -d 99 -f bnics05_pen -i 135 -p 1.0.0.175.bnics05_pen.penguin.erdf.s1 -s -w

# BSP Build ENV Setup on Ubuntu
http://pawiki.barnesandnoble.com/confluence/pages/viewpage.action?pageId=27004852

# Gerrit Repo Development
http://pawiki.barnesandnoble.com/confluence/pages/viewpage.action?pageId=6258989

# 
repo init -u ssh://platextgit03.barnesandnoble.com:29418/nvidia/manifest -b nookjb04 --reference=/home/jfeng/Project/ext/nookjb


# setup repo server
http://wenku.baidu.com/view/2b736a05b84ae45c3b358cca.html

